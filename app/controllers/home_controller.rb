class HomeController < ApplicationController
  def index
  end
  def documents
  end
  def send_mail
    @user = params[:user]
    @person = params[:person]
    @email = @user[:address]
    @name = @person[:name]
    @spec = @person[:spec_id]
    @form = @person[:forma_id]
    SendEmailJob.set(wait: 20.seconds).perform_later(@email, @name, @spec, @form)
    respond_to do |format|
      format.js {render inline: "location.reload();" }
    end
  end
end
