class UserMailer < ApplicationMailer
	 default from: 'priem@mgpet.ru'

  def welcome_email(email, name, spec, form)
    @email = email
    @name = name
    @spec = spec
    @form = form
    mail(to: @email, subject: 'Спасибо, что вы выбрали наш техникум!')
  end
end
