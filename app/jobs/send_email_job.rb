class SendEmailJob < ApplicationJob
  queue_as :default

  def perform(email, name, spec, form)
    @email = email
    @name = name
    @spec = spec
    @form = form
    UserMailer.welcome_email(@email, @name, @spec, @form).deliver_later
  end
end
